import React from "react";

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                © 2020  Alicja Sadowska
            </div>
        );
    }
}

export default Footer;
