import React from "react";
import axios from "axios";
import PopupQuiz from "./PopupQuiz.js";
import WordQuiz from "./WordQuiz.js";

class Quiz extends React.Component {

    constructor(props) {
        super(props);
        this.state = {showPopup: true, fetched: false, selectedIndex: 0, participant: '', questions: [], answer: ''};
    }

    togglePopup(childState) {
        if (childState.participant === '') {
            alert("Nie podałeś imienia!");
            window.location.reload(true)
        }
        let checkedCategories = childState.categories
            .filter(category => category.isChecked)
            .map(category => category.id);
        this.getWordsByCategoryIds(checkedCategories);
        this.setState({
            showPopup: !this.state.showPopup,
            participant: childState.participant
        });
    }

    getWordsByCategoryIds(categoryIds) {
        if (categoryIds.length === 0) {
            alert("Nie wybrałeś kategorii!");
            window.location.reload(true)
        }
        const params = new URLSearchParams();
        categoryIds.forEach(categoryId => {
            params.append("categoryId", categoryId);
        });
        axios.get(`http://localhost:8080/api/v1/results/quiz`, {
            params: params
        }).then(res => {
            const questions = res.data;
            this.setState({questions: questions, selectedIndex: 0, fetched: true})
        })
    }

    _ToggleNext = () => {
        if(this.state.selectedIndex === this.state.questions.length - 1)
            return;

        const questions = this.state.questions;
        questions[this.state.selectedIndex].answer = this.state.answer;

        if (this.state.answer === '') {
            alert("Nie odpowiedziałeś na pytanie!");
        }
        else
        {
            this.setState(prevState => ({
                selectedIndex: prevState.selectedIndex + 1,
                questions: questions,
                answer: ''
            }))
        }
    };

    _ToggleSubmit = () => {

        const questions = this.state.questions;
        questions[this.state.selectedIndex].answer = this.state.answer;

        this.setState(prevState => ({
            questions: questions,
            answer: ''
        }));

        const params = new URLSearchParams();
        params.append("participant", this.state.participant);
        axios.post(`http://localhost:8080/api/v1/results/quiz`, this.state.questions, {
            params: params
        })
            .then(res => {
                alert(`Użytkowniku ${res.data.participant} uzyskałeś ${res.data.result} punktów` );
                window.location.reload(true);
            })
    };

    handleChangeAnswer = event => {
        this.setState({answer: event.target.value})
    };

    handleChangeParticipant = event => {
        this.setState({participant: event.target.value})
    };

    render() {
        return (
            <div className="main">
                {this.state.showPopup ?
                    <PopupQuiz handleChange={this.handleChangeParticipant} closePopup={this.togglePopup.bind(this)}/>
                    : <div>
                        {this.state.selectedIndex + 1 === this.state.questions.length ?
                            <div className="buttonright-hld"><button className="button" onClick={this._ToggleSubmit}>Zapisz</button></div> :
                            <div className="buttonright-hld"><button className="button" onClick={this._ToggleNext}>Następny</button></div>}
                        {this.state.fetched ? <WordQuiz value = {this.state.answer} handleChange={this.handleChangeAnswer} {...this.state.questions[this.state.selectedIndex]}/> : ''}
                    </div>
                }


            </div>
        );
    }
}

export default Quiz;
