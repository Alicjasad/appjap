import React from "react";

class WordQuiz extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="wordcontainer">
                <div className="top">{this.props.question}</div>
                <div className="bottom">
                    <input onChange={this.props.handleChange} value={this.props.value} />
                </div>
            </div>
        )
    }
}

export default WordQuiz;