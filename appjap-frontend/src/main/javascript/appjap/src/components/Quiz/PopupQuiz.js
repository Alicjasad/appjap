import React from 'react';
import "../../assets/appjap.css"
import axios from "axios";
import Checkbox from "../Checkbox.js";

class PopupQuiz extends React.Component {

    state = {
        categories: [],
        participant: ''
    };

    componentDidMount() {
        axios.get('http://localhost:8080/api/v1/categories')
            .then(res => {
                const categories = res.data;
                this.addIsChecked(categories);
            })
    }

    addIsChecked = (categories) => {
        const myCategories = categories.map(category => {
            return {id: category.id, name: category.name, isChecked: false}
        });
        this.setState({categories: myCategories})
    };

    handleCheckChildElement = (event) => {
        let categories = this.state.categories;
        categories.forEach(category => {
            if (category.name === event.target.value)
                category.isChecked = event.target.checked
        });
        this.setState({categories: categories})
    };
    handleChange = event => {
        this.setState({participant: event.target.value})
    };

    render() {
        return (
            <div className='popup'>

                <div className="category">Wybierz Kategorie:</div>

                {this.state.categories.map(category => {
                    return (
                        <div className="checkboxes">
                        <Checkbox handleCheckChildElement={this.handleCheckChildElement}
                                  key={category.id} {...category}/>
                        </div>)
                })}

                <label className="name-hld"><a className="mr-3">Podaj imię:</a>
                    <input type="text" name="participant" onChange={this.handleChange}/>
                </label>
                <div className="btn-holder">
                    <button className="button" onClick={() => this.props.closePopup(this.state)}>Dalej</button>
                </div>
            </div>
        );
    }
}

export default PopupQuiz;