import React from "react";
import PopupLearning from "./PopupLearning.js";
import axios from "axios";
import WordLearning from "./WordLearning";

class Learning extends React.Component {

    constructor(props) {
        super(props);
        this._TogglePrev = this._TogglePrev.bind(this);
        this._ToggleNext = this._ToggleNext.bind(this);
        this.state = {showPopup: true, fetched: false, words: [], selectedIndex: 0};
    }

    togglePopup(categories) {
        let checkedCategories = categories
            .filter(category => category.isChecked)
            .map(category => category.id);
        this.getWordsByCategoryIds(checkedCategories);
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    getWordsByCategoryIds(categoryIds) {
        if (categoryIds.length === 0) {
            alert("Nie wybrałeś kategorii!");
            window.location.reload(true)
        }
        const params = new URLSearchParams();
        categoryIds.forEach(categoryId => {
            params.append("categoryId", categoryId);
        });
        axios.get(`http://localhost:8080/api/v1/categories/words`, {
            params: params
        }).then(res => {
            const words = res.data;
            this.setState({words: words, selectedIndex: 0, fetched: true})
        })
    }

    _ToggleNext() {
        if (this.state.selectedIndex === this.state.words.length - 1)
            return;

        this.setState(prevState => ({
            selectedIndex: prevState.selectedIndex + 1
        }))
    }

    _TogglePrev() {
        if (this.state.selectedIndex === 0)
            return;

        this.setState(prevState => ({
            selectedIndex: prevState.selectedIndex - 1
        }))
    }


    render() {
        return (
            <div className="main">
                {this.state.showPopup ?
                    <PopupLearning closePopup={this.togglePopup.bind(this)}/>
                    :
                    <div>
                        {this.state.selectedIndex === 0 ?
                            <div>
                                <div className="buttonright-hld">
                                    <button className="button" onClick={this._ToggleNext}>Następny</button>
                                    {this.state.fetched ?
                                        <WordLearning {...this.state.words[this.state.selectedIndex]}/> : ''}
                                </div>
                            </div>
                            :
                            <div>
                                {this.state.selectedIndex === this.state.words.length - 1 ?
                                    <div>
                                        <div className="buttonleft-hld">
                                            <button className="button" onClick={this._TogglePrev}>Poprzedni</button>
                                            {this.state.fetched ?
                                                <WordLearning {...this.state.words[this.state.selectedIndex]}/> : ''}
                                        </div>
                                    </div>
                                    :
                                    <div>
                                        <div className="buttonleft-hld">
                                            <button className="button" onClick={this._TogglePrev}>Poprzedni</button>
                                        </div>
                                        <div className="buttonright-hld">
                                            <button className="button" onClick={this._ToggleNext}>Następny</button>
                                            {this.state.fetched ?
                                                <WordLearning {...this.state.words[this.state.selectedIndex]}/> : ''}
                                        </div>
                                    </div>
                                }

                            </div>
                        }

                    </div>
                }
            </div>

        );


    }


}


export default Learning;
