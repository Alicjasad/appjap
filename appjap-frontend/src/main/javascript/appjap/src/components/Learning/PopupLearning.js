import React from 'react';
import "../../assets/appjap.css"
import axios from "axios";
import Checkbox from "../Checkbox.js";

class PopupLearning extends React.Component {

    state = {
        categories: []
    };

    componentDidMount() {
        axios.get('http://localhost:8080/api/v1/categories')
            .then(res => {
                const categories = res.data;
                this.addIsChecked(categories);
            })
    }

    addIsChecked = (categories) => {
        const myCategories = categories.map(category => {
            return {id: category.id, name: category.name, isChecked: false}
        });
        this.setState({categories: myCategories})
    };

    handleCheckChildElement = (event) => {
        let categories = this.state.categories;
        categories.forEach(category => {
            if (category.name === event.target.value)
                category.isChecked = event.target.checked
        });
        this.setState({categories: categories})
    };

    render() {
        return (
            <div className='popup'>
                <div className="category" >Wybierz Kategorie:</div>
                {this.state.categories.map(category => {
                    return (
                        <div className="checkboxes">
                        <Checkbox handleCheckChildElement={this.handleCheckChildElement} key={category.id} {...category}/>
                        </div>
                    )})}
                <div className="btn-holder">
                    <button className="button" onClick={ () => this.props.closePopup(this.state.categories)}>Dalej</button>
                </div>
            </div>
        );
    }
}

export default PopupLearning;