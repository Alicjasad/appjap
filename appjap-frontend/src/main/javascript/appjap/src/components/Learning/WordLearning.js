import React from "react";

const WordLearning = (props) => {
    return (
        <div className="wordcontainer">
            <div className="top">{props.foreignLanguage}</div>
            <div className="bottom">{props.nativeLanguage}</div>
        </div>
    )
};

export default WordLearning;