
import React from 'react'

export const Checkbox = props => {
    return (
        <ul>
            <input onClick={props.handleCheckChildElement} type="checkbox" checked={props.isChecked} value={props.name} /> {props.name}
        </ul>
    )
}

export default Checkbox