import React from "react";
import routes from "../routes.js";
import NavItem from "reactstrap/es/NavItem";
import { NavLink } from "react-router-dom";
import { Container } from "react-bootstrap";
import "../assets/appjap.css"


class Sidebar extends React.Component {
    render() {
        return (
            <>
                <>
                    <nav className="sidenav">
                        <img src={require("../assets/sakura.png")}/>
                    <Container fluid>
                        <nav className="brandName"> {this.props.brandText} </nav>
                        {this.createLinks(routes)}
                    </Container>
                    </nav>
                </>
            </>
        );
    }

    createLinks = routes => {
        return routes.map((prop) => {
            return (
                <NavItem>
                    <NavLink
                        to={prop.path}
                        tag={NavLink}
                    >
                        <i className={prop.icon} />
                        {prop.name}
                    </NavLink>
                </NavItem>
            );
        });
    };

}

export default Sidebar;
