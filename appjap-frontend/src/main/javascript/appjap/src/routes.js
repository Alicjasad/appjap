import Index from "./views/Index.js";
import Learning from "./components/Learning/Learning.js";
import Quiz from "./components/Quiz/Quiz.js";
import Result from "./views/Result.js";


var routes = [
    {
        path:"/index",
        name:"Strona Główna",
        component: Index,
        icon:"icon-ha"
    },
    {
        path:"/learning",
        name:"Nauka",
        component: Learning,
        icon:"icon-yu"
    },
    {
        path:"/quiz",
        name:"Test",
        component: Quiz,
        icon:"icon-yo"
    },
    {
        path:"/result",
        name:"Wyniki",
        component: Result,
        icon:"icon-hiragana"
    },
];
export default routes;