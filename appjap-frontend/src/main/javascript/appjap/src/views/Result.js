import React from "react";
import axios from 'axios';

class Result extends React.Component {
    state = {
        results: []
    };

    componentDidMount() {
        axios.get('http://localhost:8080/api/v1/results')
            .then(res => {
                const results = res.data
                this.setState({results});
            })
    }

    render() {
        return (
            <div className="main">
                <table className= "table">
                        <tr className="tabletop">
                            <td> Imię </td>
                            <td> Wynik </td>
                        </tr>
                {this.state.results.map(result =>
                        <tr>
                            <td>  {result.participant} </td>
                            <td> {result.result} </td>
                        </tr>
                )}
                    </table>
            </div>
        );
    }
}

export default Result;
