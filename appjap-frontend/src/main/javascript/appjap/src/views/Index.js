import React from "react";

class Index extends React.Component {
    render() {
        return (
            <div className="index">
                <div className="hello">Witaj!</div>
                <div>Oto aplikacja, która została stworzona w celu pomocy w nauce języka japońskiego. Jeśli chcesz w prosty i łatwy sposób przyswoić podstawowe słowa w języku mieszkańców kraju kwitnącej wiśni to pozwól, że przedstawię sposób działania aplikacji:
                   <ul className="p-3">
                    <li className="list">W zakładce <b> Nauka </b> znajdziesz listę, z której możesz wybrać interesujące Cię kategorie. Następnie przechodzimy do słów zapisanych w fiszkach, które ułatwią Ci naukę.</li>
                    <li className="list">W zakładce <b>Test</b> znajdziesz listę z kategoriami, z których chcesz wykonać test. Pamiętaj o wpisaniu swojego imienia! Quiz składa się z 10 pytań i sprawdzi Twój postęp w nauce.</li>
                    <li className="list">W zakładce <b>Wyniki</b> znajduje się tabela wyników. Dzięki niej w łatwy sposób sprawdzisz jak poszedł Ci test i porównasz go na tle innych użytkowników.</li>
                   </ul>
                    <div className="hello">Powodzenia!</div>
                </div>
            </div>
        );
    }
}

export default Index;
