import React from "react";
import Switch from "react-router-dom/es/Switch";
import routes from "../routes.js";
import Redirect from "react-router-dom/es/Redirect";
import Route from "react-router-dom/es/Route";
import Footer from "../components/Footer.js";
import Sidebar from "../components/Sidebar.js";


class Layout extends React.Component {
    render() {
        return (
<>
    <Sidebar
        {...this.props}
        brandText={this.getBrandText(this.props.location.pathname)}
        routes={routes}
    />
    <div>
            <Switch>
                {this.getRoutes(routes)}
                <Redirect from="*" to="/index" />
            </Switch>
            <Footer/>
    </div>
    </>
        );
    }
    getRoutes = routes => {
        return routes.map((prop) => {
                return (
                    <Route
                        path={prop.path}
                        component={prop.component}

                    />
                );
        });

    };
    getBrandText = path => {
        for (let i = 0; i < routes.length; i++) {
            if (
                this.props.location.pathname.indexOf(
                    routes[i].path
                ) !== -1
            ) {
                return routes[i].name;
            }
        }
    };
}

export default Layout;
