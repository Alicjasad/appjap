import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import * as ReactDOM from "react-dom";
import Layout from "./layouts/Layout.js";
import 'bootstrap/dist/css/bootstrap.css';


ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route render={props => <Layout {...props} />} />
            <Redirect from="/" to="/index" />
        </Switch>
    </BrowserRouter>,
  document.getElementById('root')
);

