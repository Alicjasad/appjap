package pl.appjap.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.appjap.app.dtos.CategoryDto;
import pl.appjap.app.model.Category;
import pl.appjap.app.repositories.CategoryRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryDto> findAll() {
        List<Category> categories = categoryRepository.findAll();
        //MAPPING
        List<CategoryDto> results = new ArrayList<>();
        for (Category category : categories) {
            results.add(this.mapToDto(category));
        }
        return results;
    }

    @Override
    public CategoryDto findById(Long id) {
        Optional<Category> category = categoryRepository.findById(id);
        Category category1 = category.orElseThrow(RuntimeException::new);
        return this.mapToDto(category1);
    }

    @Override
    public CategoryDto createCategory(CategoryDto categoryDto) {
        //Mapuje na encje
        Category category = this.mapToEntity(categoryDto);
        //zapisuje w bazie danych
        category = categoryRepository.save(category);
        //Mapuje na Dto
        return this.mapToDto(category);
    }

    @Override
    public CategoryDto updateCategory(CategoryDto categoryDto, Long id) {
        //Znajdź encje(Caletgory) o id
        Category category = categoryRepository.findById(id)
                .orElseThrow(RuntimeException::new);
        //aktualizuj encje
        category.setName(categoryDto.getName());
        //Zapisz encje
        category = categoryRepository.save(category);
        //Zwróc encje -> mapuj do category dto
        return this.mapToDto(category);
    }

    @Override
    public void deleteCategory(Long id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(RuntimeException::new);
        categoryRepository.delete(category);
    }

    private Category mapToEntity(CategoryDto category) {
        Category result = new Category();

        String name = category.getName();
        result.setName(name);

        return result;
    }

    private CategoryDto mapToDto(Category category) {
        CategoryDto result = new CategoryDto();

        result.setId(category.getId());
        result.setName(category.getName());

        return result;
    }


}
