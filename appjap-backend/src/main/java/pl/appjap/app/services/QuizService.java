package pl.appjap.app.services;

import pl.appjap.app.dtos.QuestionDto;

import java.util.List;

public interface QuizService {
    List<QuestionDto> startQuiz(List<Long> categoryIds);
    Long getScore(List<QuestionDto> questionDtos) ;
}
