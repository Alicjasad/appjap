package pl.appjap.app.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.appjap.app.dtos.CategoryDto;
import pl.appjap.app.dtos.QuestionDto;
import pl.appjap.app.dtos.WordDto;
import pl.appjap.app.model.Result;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class QuizServiceImpl implements QuizService {

    private final WordService wordService;

    @Override
    public List<QuestionDto> startQuiz(List<Long> categoryIds) {
        List<WordDto> wordList = wordService.findAllByCategoryIds(categoryIds);
        return getQuestions(wordList, 10);
    }

    @Override
    public Long getScore(List<QuestionDto> questionDtos) {
        Long score = 0L;
        for (QuestionDto questionDto : questionDtos) {
            String answer = questionDto.getAnswer();
            WordDto wordDto = wordService.findById(questionDto.getWordId());
            if (answer.equalsIgnoreCase(wordDto.getNativeLanguage())) {
                score++;
            }
        }
        return score;
    }

    private List<QuestionDto> getQuestions(List<WordDto> wordList, Integer size) {
        List<QuestionDto> questionList = new ArrayList<>();
        int index;
        Random r = new Random();
        for (int i = 0; i < size; i++) {
            index = r.nextInt(wordList.size());
            WordDto wordDto = wordList.get(index);
            questionList.add(preperMap(wordDto));
        }
        return questionList;
    }

    private QuestionDto preperMap(WordDto wordDto) {
        QuestionDto questionDto = new QuestionDto();
        questionDto.setWordId(wordDto.getId());
        questionDto.setQuestion(wordDto.getForeignLanguage());
        return questionDto;
    }
}
