package pl.appjap.app.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.appjap.app.dtos.ResultDto;
import pl.appjap.app.model.Result;
import pl.appjap.app.repositories.ResultRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ResultServiceImpl implements ResultService {

    private final ResultRepository resultRepository;

    @Override
    public List<ResultDto> findAll() {
        return resultRepository.findAll().stream()
                .map(this::mapToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ResultDto createResult(ResultDto resultDto) {
        Result result = this.mapToEntity(resultDto);
        resultRepository.save(result);
        return this.mapToDto(result);
    }

    private Result mapToEntity(ResultDto resultDto) {
        Result result = new Result();
        result.setParticipant(resultDto.getParticipant());
        result.setResult(resultDto.getResult());

        return result;
    }

    private ResultDto mapToDto(Result result) {
        ResultDto resultDto = new ResultDto();
        resultDto.setParticipant(result.getParticipant());
        resultDto.setResult(result.getResult());

        return resultDto;
    }
}
