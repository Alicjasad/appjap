package pl.appjap.app.services;

import pl.appjap.app.dtos.ResultDto;

import java.util.List;

public interface ResultService {
    ResultDto createResult(ResultDto resultDto);
    List<ResultDto> findAll();
}
