package pl.appjap.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.appjap.app.dtos.WordDto;
import pl.appjap.app.model.Category;
import pl.appjap.app.model.Word;
import pl.appjap.app.repositories.CategoryRepository;
import pl.appjap.app.repositories.WordRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WordServiceImpl implements WordService {

    private final WordRepository wordRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public WordServiceImpl(WordRepository wordRepository, CategoryRepository categoryRepository) {
        this.wordRepository = wordRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<WordDto> findAllByCategoryIds(List<Long> ids) {
        List<WordDto> wordList = new ArrayList<>();
        ids.forEach(id -> {
            wordList.addAll(findAllByCategoryId(id));
        });
        return wordList;
    }

    private List<WordDto> findAllByCategoryId(Long id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        Category category = optionalCategory.orElseThrow(RuntimeException::new);
        List<Word> words = category.getWords();
        List<WordDto> results = new ArrayList<>();
        for (Word word : words) {
            results.add(this.mapToDo(word));
        }
        return results;
    }

    @Override
    public WordDto findById(Long id) {
        Optional<Word> word = wordRepository.findById(id);
        Word word1 = word.orElseThrow(RuntimeException::new);
        return this.mapToDo(word1);
    }

    @Override
    public WordDto createWord(WordDto wordDto, Long id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        Category category = optionalCategory.orElseThrow(RuntimeException::new);
        Word word = this.mapToEntity(wordDto);
        category.getWords().add(word);
        categoryRepository.save(category);
        return this.mapToDo(word);
    }

    @Override
    public WordDto updateWord(WordDto wordDto, Long id) {
        Word word = wordRepository.findById(id)
                .orElseThrow(RuntimeException::new);
        word.setForeignLanguage(wordDto.getForeignLanguage());
        word.setNativeLanguage(wordDto.getNativeLanguage());
        word = wordRepository.save(word);
        return this.mapToDo(word);
    }

    @Override
    public void deleteWord(Long id) {
        Word word = wordRepository.findById(id)
                .orElseThrow(RuntimeException::new);
        wordRepository.delete(word);
    }

    private WordDto mapToDo(Word word) {
        WordDto result = new WordDto();

        result.setId(word.getId());
        result.setForeignLanguage(word.getForeignLanguage());
        result.setNativeLanguage(word.getNativeLanguage());

        return result;
    }

    private Word mapToEntity(WordDto word) {
        Word result = new Word();

        String foreignLanguage = word.getForeignLanguage();
        result.setForeignLanguage(foreignLanguage);
        String nativeLanguage = word.getNativeLanguage();
        result.setNativeLanguage(nativeLanguage);

        return result;
    }
}