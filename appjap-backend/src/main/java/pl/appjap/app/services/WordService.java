package pl.appjap.app.services;

import pl.appjap.app.dtos.WordDto;

import java.util.List;

public interface WordService {
    List<WordDto> findAllByCategoryIds(List<Long> ids);
    WordDto findById(Long id);
    WordDto createWord(WordDto wordDto, Long id);
    WordDto updateWord(WordDto wordDto, Long id);
    void deleteWord(Long id);

}
