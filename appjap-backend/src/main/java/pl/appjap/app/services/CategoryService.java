package pl.appjap.app.services;

import pl.appjap.app.dtos.CategoryDto;

import java.util.List;

public interface CategoryService {
    List<CategoryDto> findAll();
    CategoryDto findById(Long id);
    CategoryDto createCategory(CategoryDto category);
    CategoryDto updateCategory(CategoryDto categoryDto, Long id);
    void deleteCategory(Long id);

}
