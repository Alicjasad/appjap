package pl.appjap.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.appjap.app.model.Word;

public interface WordRepository extends JpaRepository<Word, Long> {
}
