package pl.appjap.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.appjap.app.model.Result;

public interface ResultRepository extends JpaRepository<Result, Long> {
}
