package pl.appjap.app.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.appjap.app.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
