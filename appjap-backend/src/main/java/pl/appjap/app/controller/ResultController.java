package pl.appjap.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.appjap.app.dtos.QuestionDto;
import pl.appjap.app.dtos.ResultDto;
import pl.appjap.app.services.QuizService;
import pl.appjap.app.services.ResultService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/results")
@RequiredArgsConstructor
public class ResultController {

    private final ResultService resultService;
    private final QuizService quizService;

    @GetMapping
    public ResponseEntity<List<ResultDto>> findAll() {
        List<ResultDto> resultDtos = resultService.findAll();
        return ResponseEntity.ok(resultDtos);
    }

    @GetMapping("/quiz")
    public ResponseEntity<List<QuestionDto>> startQuiz(@RequestParam("categoryId") List<Long> ids) {
        List<QuestionDto> questionDtos = quizService.startQuiz(ids);
        return ResponseEntity.ok(questionDtos);
    }

    @PostMapping("/quiz")
    public ResponseEntity<ResultDto> countScore(@RequestBody List<QuestionDto> questionDtos,@RequestParam("participant") String participant) {
        Long score = quizService.getScore(questionDtos);
        ResultDto resultDto = new ResultDto();
        resultDto.setResult(score);
        resultDto.setParticipant(participant);
        resultDto = resultService.createResult(resultDto);

        return ResponseEntity.ok(resultDto);
    }
}
