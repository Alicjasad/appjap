package pl.appjap.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.appjap.app.dtos.WordDto;
import pl.appjap.app.services.WordService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
public class WordController {

    private final WordService wordService;

    @Autowired
    public WordController(WordService wordService) {
        this.wordService = wordService;
    }

    @GetMapping("/words")
    public ResponseEntity<List<WordDto>> findAllByCategoryIds(@RequestParam("categoryId") List<Long> ids) {
        List<WordDto> results = wordService.findAllByCategoryIds(ids);
        return ResponseEntity.ok(results);
    }

    @GetMapping("/{categoryId}/words/{id}")
    public ResponseEntity<WordDto> findById(@PathVariable Long id, @PathVariable String categoryId) {
        WordDto result = wordService.findById(id);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/{categoryId}/words")
    public ResponseEntity<WordDto> createWord(@RequestBody WordDto word, @PathVariable("categoryId") Long categoryId) {
        WordDto result = wordService.createWord(word, categoryId);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/{categoryId}/words/{id}")
    public ResponseEntity<WordDto> updateWord(@RequestBody WordDto wordDto, @PathVariable Long id, @PathVariable String categoryId) {
        if (IsRequestCorrect(id, wordDto.getId())) {
            WordDto result = wordService.updateWord(wordDto, id);
            return ResponseEntity.ok(result);
        } else {
            return  ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{categoryId}/words/{id}")
    public ResponseEntity<WordDto> deleteWord(@PathVariable Long id, @PathVariable String categoryId) {
        wordService.deleteWord(id);
        return ResponseEntity.ok().build();
    }

    private boolean IsRequestCorrect(Long idPath, Long id) {
        if (id == null) {
            return false;
        }
        if (idPath == null) {
            return false;
        }
        return idPath.equals(id);
    }
}
