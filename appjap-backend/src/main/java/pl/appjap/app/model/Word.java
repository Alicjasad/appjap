package pl.appjap.app.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;


@Entity
@Table(name = "WORDS")
@Getter
@Setter
@NoArgsConstructor
public class Word extends AbstractEntity{

    @Column(name = "FOREIGN_LANGUAGE")
    @NotEmpty
    private String foreignLanguage;

    @Column(name = "NATIVE_LANGUAGE")
    @NotEmpty
    private String nativeLanguage;
}
