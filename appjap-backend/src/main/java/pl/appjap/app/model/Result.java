package pl.appjap.app.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "RESULTS")
public class Result extends AbstractEntity {

    @Column(name = "PARTICIPANT")
    @NotEmpty
    private String participant;

    @Column(name = "RESULT")
    @NotNull
    private Long result;


}
