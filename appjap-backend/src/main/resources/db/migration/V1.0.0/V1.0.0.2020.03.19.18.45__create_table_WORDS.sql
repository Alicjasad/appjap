CREATE TABLE WORDS
(
    id               bigserial PRIMARY KEY,
    foreign_language varchar(30) NOT NULL,
    native_language  varchar(30) NOT NULL,
    category_id      bigint      NOT NULL,

    VERSION          int,
    CREATED          timestamp,
    CREATED_BY       varchar(100),
    UPDATED           timestamp,
    UPDATED_BY        varchar(100),

    CONSTRAINT category_id_fk foreign key (category_id) REFERENCES CATEGORIES (id)
);
CREATE UNIQUE INDEX WORDS_fln_uindex ON WORDS (foreign_language);
