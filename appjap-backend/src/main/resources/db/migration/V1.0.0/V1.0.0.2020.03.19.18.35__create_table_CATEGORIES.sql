CREATE TABLE CATEGORIES
(
    id bigserial PRIMARY KEY,
    name varchar(30) NOT NULL,

    VERSION int,
    CREATED timestamp,
    CREATED_BY varchar(100),
    UPDATED timestamp,
    UPDATED_BY varchar(100)
);
CREATE UNIQUE INDEX CATEGORIES_name_uindex ON CATEGORIES (name);