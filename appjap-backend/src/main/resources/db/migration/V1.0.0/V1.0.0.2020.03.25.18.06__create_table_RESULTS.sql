CREATE TABLE RESULTS
    (
        id               bigserial PRIMARY KEY,
        participant varchar(30) NOT NULL,
        result      bigint      NOT NULL,

        VERSION          int,
        CREATED          timestamp,
        CREATED_BY       varchar(100),
        UPDATED           timestamp,
        UPDATED_BY        varchar(100)
);


